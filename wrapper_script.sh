#!/bin/ash

sigint() {
    echo "SIGINT received"
    pkill -INT sshd
    pkill -INT darkhttpd
    exit
}
trap sigint INT

sigterm() {
    echo "SIGTERM received"
    pkill -TERM sshd
    pkill -TERM darkhttpd
    exit
}
trap sigterm TERM

echo "Start httpd"
/usr/bin/darkhttpd /var/www/localhost/htdocs --daemon
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start httpd: $status"
  exit $status
fi
echo "httpd started"

echo "Start sshd"
/usr/sbin/sshd
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start sshd: $status"
  exit $status
fi
echo "sshd started"

while :; do
    sleep 1s
done
