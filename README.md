# Build

docker login gitlab-research.centralesupelec.fr:4567

docker build -t gitlab-research.centralesupelec.fr:4567/marcadet/sip_shell_escape_docker_image .

docker push gitlab-research.centralesupelec.fr:4567/marcadet/sip_shell_escape_docker_image

# Run
-- In a terminal

docker run --rm --name sip_shell_escape_running gitlab-research.centralesupelec.fr:4567/marcadet/sip_shell_escape_docker_image

-- In another terminal

docker exec -ti sip_shell_escape_running login -f level0

docker stop sip_shell_escape_running
