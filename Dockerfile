# Cannot add ssh server to busybox
#FROM busybox:latest
FROM alpine:latest
RUN apk add --no-cache openssh
RUN /usr/bin/ssh-keygen -A
RUN echo AuthenticationMethods none >>/etc/ssh/sshd_config
RUN echo PermitEmptyPasswords yes >>/etc/ssh/sshd_config
# contrary to busybox, alpine needs an http server
RUN apk add --no-cache darkhttpd
# remove message on initial login
RUN echo "" >/etc/motd
#
# do not use ENV, otherwise variables get defined in the image
ARG password_level0=
ARG password_level1=let_me_pass_please
ARG password_level2=you_found_me_yeah
ARG password_level3=I_am_finally_reunited
ARG password_level4=I_was_stolen
ARG password_level5=zip_is_not_the_only_one
ARG password_level6=always_look_at_PATH
ARG password_root="This was a triumph, huge_success"
# Allow su to everyone
RUN chmod +s /bin/su
RUN echo "alias su='/bin/su -l'" >/etc/profile
RUN echo "export PS1='\u \$ '" >>/etc/profile
# use ENV for displaying message when root
RUN echo "export ENV=/root/.shinit" >>/etc/profile
#
RUN adduser -D level0
RUN echo level0:${password_level0} | chpasswd
RUN adduser -D level1
RUN echo level1:${password_level1} | chpasswd
RUN adduser -D level2
RUN echo level2:${password_level2} | chpasswd
RUN adduser -D level3
RUN echo level3:${password_level3} | chpasswd
RUN adduser -D level4
RUN echo level4:${password_level4} | chpasswd
RUN adduser -D level5
RUN echo level5:${password_level5} | chpasswd
RUN adduser -D level6
RUN echo level6:${password_level6} | chpasswd
RUN echo root:${password_root} | chpasswd
#
# level0
#
USER level0
WORKDIR /home/level0
RUN mkdir weird
RUN echo "echo -n Your mission, if you choose to accept it:"                        >.profile
RUN echo "echo \" infiltrate that machine and gain total control (become root).\"" >>.profile
RUN echo "echo The first step is to find the password for level1."                 >>.profile
RUN echo "echo Type exit if you don\'t want to take on this challenge."            >>.profile
RUN echo "cd weird"                                                                >>.profile
RUN echo -n "OK, this one was quite simple, here is the password to level1: " >README.txt
RUN echo ${password_level1}                                                  >>README.txt
RUN echo "Use it with su level1"                                             >>README.txt
#
# level1
#
USER root
RUN mkdir /var/level1
RUN chown level1:level1 /var/level1
USER level1
WORKDIR /var/level1
RUN echo "I am lost again, let's go home" >README.txt
RUN echo ${password_level2}               >.password_to_level2
WORKDIR /home/level1
RUN echo "cd /var/level1"                                     >.profile
RUN echo "May be I forgot something before getting back home" >README.txt
#
# level2
#
USER level2
WORKDIR /home/level2
# no zip on BusyBox
#RUN echo ${password_level3}   >password_to_level3.txt
#RUN zip password_to_level3.zip password_to_level3.txt
#RUN rm password_to_level3.txt
COPY password_to_level3.zip .
RUN split -b 50 -a 1 password_to_level3.zip password_to_level3.zip.part.
RUN rm password_to_level3.zip
RUN mkdir -p foo/bar/missing_part
RUN mv password_to_level3.zip.part.e foo/bar/missing_part/
RUN echo "cat is concatenate, redirect output is >, unzip is unzip" >README.txt
#
# level3
#
USER level3
WORKDIR /home/level3
RUN echo ${password_level4} >password_to_level4.txt
RUN chmod 000 password_to_level4.txt
USER root
RUN chown level2:level2 password_to_level4.txt
USER level3
RUN echo "The owner can change the permissions with chmod, exit to leave current account" >README.txt
#
# level4
#
USER level4
WORKDIR /home/level4
RUN echo ${password_level5} >password_to_level5.txt
RUN base64 password_to_level5.txt >password_to_level5.txt.b64
RUN rm password_to_level5.txt
RUN bzip2 password_to_level5.txt.b64
RUN echo "zip is not the only one" >README.txt
#
# level5
#
USER level5
WORKDIR /home/level5
RUN echo ${password_level6} >password_to_level6.txt
RUN echo "export PATH=/badbin" >.profile
RUN echo "echo echo \\\$PATH ? export PATH=..." >>.profile
#
# level6
#
USER level6
WORKDIR /home/level6
RUN echo "echo What is an alias ?" >.profile
RUN echo "echo bye !"             >>.profile
RUN echo "exit"                   >>.profile
RUN echo "The next and last level is root, but where is the password ?"  >README.txt
RUN echo "Ask to the web server." >>README.txt
USER root
ENV httpd_root=/var/www/localhost/htdocs
#RUN echo "<html><body><p>get root.password</p></body></html>" >/var/www/index.html
RUN echo "<html><body><p>get root.password</p></body></html>" >${httpd_root}/index.html
#RUN chmod 640 /var/www/index.html
RUN chmod 640 ${httpd_root}/index.html
#RUN echo -n ${password_root}  >/var/www/root.password
RUN echo -n ${password_root}  >${httpd_root}/root.password
#RUN chmod 640 /var/www/root.password
RUN chmod 640 ${httpd_root}/root.password
#RUN chmod 750 /var/www
RUN chmod 750 ${httpd_root}
#
# Initial account is level0
#
USER root
WORKDIR /root
RUN echo "echo You are root, you are a winner!"                                        >.shinit
RUN echo "echo Thanks to Arnaud Clauss and Benjamin Koltes for the initial version of this escape shell." >>.shinit
#ENTRYPOINT ["/usr/bin/httpd", "-f", "-h", "/var/www"]
#ENTRYPOINT /usr/bin/darkhttpd ${httpd_root}
#ENTRYPOINT ["/usr/sbin/sshd", "-D"]
WORKDIR /usr/local/lib
COPY wrapper_script.sh wrapper_script.sh
ENTRYPOINT ["/bin/ash", "/usr/local/lib/wrapper_script.sh"]
